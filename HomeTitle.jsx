import React from 'react';
import { Text, StyleSheet } from 'react-native';

export default function HomeTitle() {
    return <Text style={styles.title} data-test="homeTitle">HomeTitleだよ</Text>;
};

const styles = StyleSheet.create({
    title: {
        color: '#555',
        fontSize: 32,
        margin: 30,
    }
});